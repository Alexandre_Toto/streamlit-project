# STREAMLIT DASHBOARD

I'm testing the Streamlit framework to understand how it works. 
Using a csv file I retrieved from the Kaggle.com website. 

## Install library :

- `pip install streamlit`
- `pip install plotly`
- `pip install pandas`

## Run server streamlit :

- `streamlit run <your file.py>`

## Screen shot :

![Alt text](capture/streamlit.png "dashboard streamlit")
