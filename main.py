import os

import streamlit as st
import plotly.express as px
import pandas as pd

# Title for the Dashboard

st.title(" :bar_chart: Dashboard test Streamlit")
st.markdown('<style>div.block-container{padding-top:1rem;}</style>', unsafe_allow_html=True)

# Uploader file in Dashboard
file = st.file_uploader(":file_folder: Upload file", type=(['csv', 'json', 'txt', 'xlsx', 'xls']))

if file is not None:
    filename = file.name
    st.write(filename)
    df = pd.read_csv(filename, on_bad_lines='skip', sep=';')
    print(df)
else:
    os.chdir(r"C:\Users\GIALETOT\Desktop\Labs\streamlit_test")
    df = pd.read_csv("imap_export.csv", on_bad_lines='skip', sep=';')

col1, col2 = st.columns(2)

# ---------------------------- FILTERED IN SIDEBAR -----------------------------------

st.sidebar.header("Chose your filter")

# Create for region
country = st.sidebar.multiselect("Pick your region", df["Country"].unique())
if not country:
    df2 = df.copy()
else:
    df2 = df[df['Country'].isin(country)]

# Create for state
state = st.sidebar.multiselect("Pick your state", df2["State"].unique())
if not state:
    df3 = df2.copy()
else:
    df3 = df2[df2["State"].isin(state)]

# Create for city
city = st.sidebar.multiselect("Pick your city", df3["City"].unique())
if not city:
    df4 = df3.copy()
else:
    df4 = df3[df3["City"].isin(city)]

# Filter the data based in country, state and city
if not country and not state and not city:
    filter_df = df
elif not state and not city:
    filter_df = df[df["Country"].isin(country)]
elif not country and not city:
    filter_df = df[df["State"].isin(state)]
elif state and city:
    filter_df = df3[df["State"].isin(state) & df3["City"].isin(city)]
elif country and city:
    filter_df = df3[df["State"].isin(country) & df3["City"].isin(city)]
elif country and state:
    filter_df = df3[df["State"].isin(country) & df3["City"].isin(state)]
elif city:
    filter_df = df3[df3["City"].isin(city)]
else:
    filter_df = df3[df3["Country"].isin(country) & df3["State"].isin(state) & df3["City"].isin(city)]

nike_df = filter_df.groupby(by=["Nike, Inc. Brand(s)"], as_index=False)["Line Workers"].sum()


with col1:
    st.subheader("Nike, Inc. Brand(s) wise Line workers")
    fig = px.bar(nike_df, x="Nike, Inc. Brand(s)", y="Line Workers",
                 text=['${:,.2f}'.format(x) for x in nike_df["Line Workers"]],
                 template="seaborn")
    st.plotly_chart(fig, use_container_width=True, height=200, width=200)

with col2:
    st.subheader("Country wise Line workers")
    fig = px.pie(filter_df, values="Line Workers", names="Country", hole=0.5)
    fig.update_traces(text=filter_df["Country"], textposition="outside")
    st.plotly_chart(fig, use_container_width=True, height=200, width=200)
